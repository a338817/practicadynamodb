const express = require('express');
const { getUsers, getUserById, addOrUpdateUser, deleteUser } = require('./dynamo');
const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: false }));


app.get('/', (req, res) => {
    res.send('hello world');
});

app.get('/users',async (req, res) => {
    try {
         const users = await getUsers(); 
         res.json(users);
    } catch (error) {
        console.log(error);
        res.status(500).json({err: "algo a ocurrido"})
    }
})

app.get('/users/:id', async (req, res) => {
    const id = req.params.id;
    try {
        const user = await getUserById(id);
        res.json(user);
    } catch (err) {
        console.error(err);
        res.status(500).json({ err: 'algo a ocurrido' });
    }
});

app.post('/users/', async (req, res) => {
    const user = req.body;
    try {
        const newUser = await addOrUpdateUser(user);
        res.json(newUser);
    } catch (err) {
        console.error(err);
        res.status(500).json({ err: 'algo a ocurrido' });
    }
});

app.put('/users/:id', async (req, res) => {
    const user = req.body;
    const { id } = req.params;
    user.id = id;
    try {
        const newUser = await addOrUpdateUser(user);
        res.json(newUser);
    } catch (err) {
        console.error(err);
        res.status(500).json({ err: 'algo a ocurrido' });
    }
});

app.delete('/users/:id', async (req, res) => {
    const { id } = req.params;
    try {
        res.json(await deleteUser(id));
    } catch (err) {
        console.error(err);
        res.status(500).json({ err: 'algo a ocurrido' });
    }
});

const port = process.env.port || 3000;
app.listen(port, () => {
    console.log(`listening on port ${port}`);
})